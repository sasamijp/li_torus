import copy
import itertools
import random
from typing import Tuple, Set, List, Optional

# Online Adaptive Fault-Tolerant Routing in 2D Torus
# の実装。論文とここではnとkが逆なので注意。

import networkx as nx

# オリジナルの型
Node = Tuple[int, ...]
Path = List[Node]


# 引数でとくに型の指定がないときはint (nとかkとかm)


def get_random_node(n, k) -> Node:
    return tuple(random.randint(0, k - 1) for _ in range(n))


def get_random_non_faulty_node(n, k, faulty_nodes: Set[Node]):
    a = get_random_node(n, k)
    while a in faulty_nodes:
        a = get_random_node(n, k)
    return a


def get_edge_as_str(a: Node, b: Node) -> str:
    return ",".join(str(a_i) for a_i in a) + \
           "-" + \
           ",".join(str(b_i) for b_i in b)


def is_faulty_edge(edge: str, faulty_set: Set[str]):
    if edge in faulty_set:
        return True
    a, b = tuple(edge.split("-"))
    return b + "-" + a in faulty_set


def generate_edge_faulty_edge_set(n, k, p_faulty: float) -> Set[str]:
    ret = set()
    n_all_edges = n * k ** n
    n_faulty_edges = n_all_edges * p_faulty
    c_faulty_edges = 0

    while c_faulty_edges < n_faulty_edges:
        a = get_random_node(n, k)
        b = random.choice(list(get_neighbors(a, n, k)))
        key = get_edge_as_str(a, b)
        if not is_faulty_edge(key, ret):
            ret.add(key)
            c_faulty_edges += 1
    return ret


def generate_node_faulty_edge_set(n, k, p_faulty: float) \
        -> Tuple[Set[str], Set[Node]]:
    set_faulty_edges = set()
    set_faulty_nodes = set()

    n_all_nodes = k ** n
    n_faulty_nodes = n_all_nodes * p_faulty

    while len(set_faulty_nodes) < n_faulty_nodes:
        a = get_random_node(n, k)

        if a not in set_faulty_nodes:
            set_faulty_nodes.add(a)
            for ne in get_neighbors(a, n, k):
                key = get_edge_as_str(a, ne)
                set_faulty_edges.add(key)

    return set_faulty_edges, set_faulty_nodes


# ノードaのi次元の方向eの近傍ノードをとる
# kを使って折返しを計算している
def get_i_dimension_neighbor(a: Node, i, e, k) -> Node:
    ret = list(copy.copy(a))
    ret[i] = (ret[i] + e + k) % k
    return tuple(ret)


# ノードaの近傍ノードを集合でとる
def get_neighbors(a: Node, n, k) -> Set[Node]:
    return {get_i_dimension_neighbor(a, i, e, k) for i, e in
            itertools.product(range(n), (1, -1))}


def get_neighbors_dimension_order(a: Node, n, k) -> List[Node]:
    return [get_i_dimension_neighbor(a, i, e, k) for i, e in
            itertools.product(range(n), (1, -1))]


# 次元iの値を入れるとlee距離を計算してくれる
def lee_dist(a_i, b_i, k):
    return min(abs(a_i - b_i), k - abs(a_i - b_i))


# n-d mesh
# ノードaがバウンダリb-Bの中にいるかどうか調べる
def is_node_in_nd_mesh(a: Node, b: Node, B: Node, n, k, m) -> bool:
    la, lb, lB = list(a), list(b), list(B)
    # b-Bの位置関係はなんでもあり
    # メッシュ内にいる ⇔ aからそれぞれのlee距離を足すとm-1になってる
    return all(lee_dist(la[i], lb[i], k) + lee_dist(la[i], lB[i], k) == m - 1
               for i in range(n))


# n-d mesh
# ノードaがバウンダリb-Bの中にいるかどうか調べる
def is_node_in_2d_mesh(a: Node, b: Node, B: Node, n, k, m, i, j) -> bool:
    la, lb, lB = list(a), list(b), list(B)
    # b-Bの位置関係はなんでもあり
    # メッシュ内にいる ⇔ aからそれぞれのlee距離を足すとm-1になってる
    return all(lee_dist(la[d], lb[d], k) + lee_dist(la[d], lB[d], k) == m - 1
               for d in (i, j))


def has_non_faulty_route(s: Node, t: Node, faulty_edges: Set[str], n,
                         k) -> bool:
    if s == t:
        return True

    open_node_queue = [ne for ne in get_neighbors(s, n, k)
                       if not is_faulty_edge(get_edge_as_str(s, ne),
                                             faulty_edges)]

    close_node_set = {s, }
    while open_node_queue:
        r = open_node_queue.pop(0)
        if r == t:
            return True

        open_neighbors = (ne for ne in get_neighbors(r, n, k)
                          if ne not in close_node_set
                          and not is_faulty_edge(get_edge_as_str(r, ne),
                                                 faulty_edges))

        for ne in open_neighbors:
            if ne == t:
                return True
            close_node_set.add(ne)
            open_node_queue.append(ne)
    return False


# バウンダリb-Bからなるメッシュ内でルーティングしてPathを返す
def bfs_in_2d_mesh(s: Node, b: Node, B: Node, faulty_edges: Set[str], n, k, m, i,
                   j,
                   t=None, t_i=None) \
        -> Optional[Path]:
    open_path_queue = [[s, ne] for ne in get_neighbors(s, n, k)
                       if is_node_in_2d_mesh(ne, b, B, n, k, m, i, j)
                       and not is_faulty_edge(get_edge_as_str(s, ne),
                                              faulty_edges)]

    close_node_set = {s, }
    while open_path_queue:
        path = open_path_queue.pop(0)
        r = path[-1]
        if t is not None and r == t:
            return path

        if (t is None and list(r)[i] == list(B)[i]) or \
                (t_i is not None and list(r)[i] == t_i):
            return path

        open_neighbors = (ne for ne in get_neighbors(r, n, k)
                          if ne not in close_node_set
                          and is_node_in_2d_mesh(ne, b, B, n, k, m, i, j)
                          and not is_faulty_edge(get_edge_as_str(r, ne),
                                                 faulty_edges)
                          )

        for ne in open_neighbors:
            new_path = copy.copy(path)
            new_path.append(ne)
            close_node_set.add(ne)

            open_path_queue.append(new_path)


def chain_routing(s: Node, t: Node, faulty_edges: set, n, k, m) \
        -> Optional[Path]:
    path = [s]
    r = copy.copy(s)

    # determine routing direction
    # n次元の1か-1のベクトルに各次元の向かうべき方を決める
    direction = [1 for _ in range(n)]
    list_r, list_t = list(r), list(t)
    for i in range(n):
        if (0 <= list_r[i] - list_t[i] <= k / 2) or \
                (0 <= list_t[i] - list_r[i] > k / 2):
            direction[i] = -1

    # n>= 4だと動かない　なんで？
    for i in range(n):
        b, B = copy.copy(list_r), copy.copy(list_r)
        # determine mesh boundaries of i and j dimensions
        j = (i + 1) % n
        b[j] = (list_r[j] - direction[j] + k) % k
        B[j] = (list_r[j] + (m - 2) * direction[j] + k) % k

        while list_r[i] != list_t[i]:
            # determine mesh boundaries of i dimension
            b[i] = list_r[i]
            B[i] = (list_r[i] + (m - 1) * direction[i] + k) % k

            if is_node_in_2d_mesh(t, tuple(b), tuple(B), n, k, m, i, j):
                print("node in mesh")
                path_bfs = bfs_in_2d_mesh(r, tuple(b), tuple(B), faulty_edges,
                                          n, k, m, i, j, t=t)
                if path_bfs is None:
                    return None
                return path + path_bfs[1:]

            print("node not in mesh")
            print(b, B)
            print(r)

            path_bfs = bfs_in_2d_mesh(r, tuple(b), tuple(B), faulty_edges,
                                      n, k, m, i, j, t_i=list_t[i])
            print(path_bfs)

            if path_bfs is None:
                return None
            path += path_bfs[1:]

            r = path_bfs[-1]
            list_r = list(r)


def adaptive_square_routing(s: Node, t: Node, faulty_edges: set, n, k, m) \
        -> Optional[Path]:
    loop_threshold = 3 * n * k
    path = [s]
    r = copy.copy(s)
    # determine routing direction

    direction = [1 for _ in range(n)]
    list_r, list_t = list(r), list(t)
    for i in range(n):
        if (0 <= list_r[i] - list_t[i] <= k / 2) or \
                (0 <= list_t[i] - list_r[i] > k / 2):
            direction[i] = -1

    while r != t:
        if len(path) > loop_threshold:
            return None

        list_r, list_t = list(r), list(t)
        i = max(range(n), key=lambda d: lee_dist(list_r[d], list_t[d], k))

        b, B = copy.copy(list_r), copy.copy(list_r)
        # determine mesh boundaries of i and j dimensions
        j = (i + 1) % n
        b[j] = (list_r[j] - direction[j] + k) % k
        B[j] = (list_r[j] + (m - 2) * direction[j] + k) % k

        # determine mesh boundaries of i dimension
        b[i] = list_r[i]
        B[i] = (list_r[i] + (m - 1) * direction[i] + k) % k

        if is_node_in_2d_mesh(t, tuple(b), tuple(B), n, k, m, i, j):
            path_bfs = bfs_in_2d_mesh(r, tuple(b), tuple(B), faulty_edges,
                                      n, k, m, i, j, t=t)
            if path_bfs is None:
                return None
            return path + path_bfs[1:]

        path_bfs = bfs_in_2d_mesh(r, tuple(b), tuple(B), faulty_edges,
                                  n, k, m, i, j, t_i=list_t[i])

        if path_bfs is None:
            return None
        path += path_bfs[1:]

        r = path_bfs[-1]


def boundary_routing(r, t, direction, faulty_edges, i, n, k, m) \
        -> Tuple[Optional[Path], Optional[bool]]:
    list_r, list_t = list(r), list(t)
    b, B = copy.copy(list_r), copy.copy(list_r)
    # determine mesh boundaries of i and j dimensions
    j = (i + 1) % n
    b[j] = (list_r[j] - direction[j] + k) % k
    B[j] = (list_r[j] + (m - 2) * direction[j] + k) % k

    # determine mesh boundaries of i dimension
    b[i] = list_r[i]
    B[i] = (list_r[i] + (m - 1) * direction[i] + k) % k

    if is_node_in_2d_mesh(t, tuple(b), tuple(B), n, k, m, i, j):
        path_bfs = bfs_in_2d_mesh(r, tuple(b), tuple(B), faulty_edges,
                                  n, k, m, i, j, t=t)

        if path_bfs is None:
            return None, None
        return path_bfs[1:], True

    path_bfs = bfs_in_2d_mesh(r, tuple(b), tuple(B), faulty_edges,
                              n, k, m, i, j, t_i=list_t[i])

    if path_bfs is None:
        return None, None
    return path_bfs[1:], False


def heuristic_square_routing(s: Node, t: Node, faulty_edges: set, n, k, m) \
        -> Optional[Path]:
    if s == t:
        return [s]
    path = [s]
    r = copy.copy(s)

    loop_threshold = 3 * n * k

    # determine routing direction
    direction = [1 for _ in range(n)]
    list_r, list_t = list(r), list(t)
    for i in range(n):
        if (0 <= list_r[i] - list_t[i] <= k / 2) or \
                (0 <= list_t[i] - list_r[i] > k / 2):
            direction[i] = -1

    while r != t:
        if len(path) > loop_threshold:
            return None

        list_r, list_t = list(r), list(t)

        i = max(range(n), key=lambda d: lee_dist(list_r[d], list_t[d], k))
        br_result, return_ok = boundary_routing(r, t, direction, faulty_edges,
                                                i, n, k, m)

        if br_result is None:
            j = (i + 1) % n
            if list_r[j] == list_t[j]:
                return None
            br_result, return_ok = boundary_routing(r, t, direction,
                                                    faulty_edges, j, n, k, m)
            # if len(br_result) == 1:
            #    print(br_result)
            if br_result is None:
                return None

        path += br_result
        if return_ok:
            return path
        r = path[-1]


def node_dist(a: Node, b: Node, n, k):
    la, lb = list(a), list(b)
    return sum(lee_dist(la[i], lb[i], k) for i in range(n))


def is_preferred_neighbor(a: Node, t: Node, a_neighbor: Node, n, k):
    return node_dist(a_neighbor, t, n, k) < node_dist(a, t, n, k)


# 一個前のノードに戻さない
def brute_routing(s: Node, t: Node, faulty_edges: set, k) -> Optional[Path]:
    r = copy.copy(s)
    path = [s]

    while r != t:
        neighbors = [ne for ne in get_neighbors_dimension_order(r, 2, k)
                     if
                     not is_faulty_edge(get_edge_as_str(r, ne), faulty_edges)
                     and (len(path) < 2 or (len(path) >= 2 and ne != path[-2]))]

        preferred_neighbors = [ne for ne in neighbors
                               if is_preferred_neighbor(r, t, ne, 2, k)]

        if preferred_neighbors:
            r = preferred_neighbors[0]
        else:
            spare_neighbors = neighbors
            if spare_neighbors:
                r = spare_neighbors[0]
            else:
                # None
                return None
        if r in path:
            # None
            return None
        path.append(r)

    return path


def brute2_routing(s: Node, t: Node, faulty_edges: set, k) -> Optional[Path]:
    r = copy.copy(s)
    path = [s]

    while r != t:
        neighbors = [ne for ne in get_neighbors(r, 2, k)
                     if
                     not is_faulty_edge(get_edge_as_str(r, ne), faulty_edges)
                     and (len(path) < 2 or ne != path[-2])]

        preferred_neighbors = [ne for ne in neighbors
                               if is_preferred_neighbor(r, t, ne, 2, k)]

        if preferred_neighbors:
            r = preferred_neighbors[0]
        else:
            spare_neighbors = neighbors
            if spare_neighbors:
                r = spare_neighbors[0]
            else:
                # None
                return None
        if r in path:
            # None
            return None
        path.append(r)

    return path


def brute3_routing(s: Node, t: Node, faulty_edges: set, n, k) -> Optional[Path]:
    r = copy.copy(s)
    path = [s]

    while r != t:
        if len(path) > 5 * n * k:
            return None
        neighbors = [ne for ne in get_neighbors(r, n, k)
                     if
                     not is_faulty_edge(get_edge_as_str(r, ne), faulty_edges)
                     and (len(path) < 2 or ne != path[-2])]

        preferred_neighbors = [ne for ne in neighbors
                               if is_preferred_neighbor(r, t, ne, n, k)]

        if preferred_neighbors:
            r = preferred_neighbors[0]
        else:
            spare_neighbors = neighbors
            if spare_neighbors:
                r = spare_neighbors[0]
            else:
                # None
                return None
        path.append(r)

    return path


def print_node_path(s, t, k, path: Path, N) -> None:
    set_path = set(path)
    for k1 in range(k):
        for k2 in range(k):
            if (k1, k2) == s:
                print('\033[93m' + "s " + '\033[0m', end="")
            elif (k1, k2) == t:
                print('\033[93m' + "t " + '\033[0m', end="")
            elif (k1, k2) in set_path:
                print("■ ", end="")
                assert (k1, k2) not in N
            else:
                if (k1, k2) in N:
                    print('\033[91m' + "☓ " + '\033[0m', end="")
                else:
                    print("□ ", end="")
        print()


def print_edge_path(s, t, k, path: Path, faulty_edges) -> None:
    set_path = set(path)

    for k1 in range(k):
        row_1 = ""
        row_2 = ""
        for k2 in range(k):
            a = (k1, k2)

            n_up = get_i_dimension_neighbor(a, 0, -1, k)
            n_left = get_i_dimension_neighbor(a, 1, -1, k)

            if is_faulty_edge(get_edge_as_str(a, n_up), faulty_edges):
                row_1 += '\033[91m' + "   *" + '\033[0m'
            else:
                if a in set_path and n_up in set_path:
                    row_1 += '\033[94m' + "   |" + '\033[0m'
                else:
                    row_1 += "   |"

            if is_faulty_edge(get_edge_as_str(a, n_left), faulty_edges):
                row_2 += '\033[91m' + "-*-" + '\033[0m'
            else:
                if a in set_path and n_left in set_path:
                    row_2 += '\033[94m' + "---" + '\033[0m'
                else:
                    row_2 += "---"

            if a == s:
                row_2 += '\033[94m' + "S" + '\033[0m'
            elif a == t:
                row_2 += '\033[94m' + "T" + '\033[0m'
            elif a in set_path:
                row_2 += '\033[94m' + "o" + '\033[0m'
            else:
                row_2 += "o"
        print(row_1)
        print(row_2)


def draw_graph(s, t, n, k, path: Path, faulty_edges):
    G = nx.Graph(ranksep=2.0)

    set_path = set(path)

    for a in itertools.product(*[list(range(k)) for _ in range(n)]):
        G.add_node(a)

    for a in itertools.product(*[list(range(k)) for _ in range(n)]):
        for i in range(n):
            ne = get_i_dimension_neighbor(a, i, -1, k)

            if is_faulty_edge(get_edge_as_str(a, ne), faulty_edges):
                if list(a)[0] == 0:
                    G.add_edge(a, ne, rank="same")
                else:
                    G.add_edge(a, ne)
                G.edges[a, ne]["color"] = "red"
                G.edges[a, ne]["penwidth"] = 3
            else:
                G.add_edge(a, ne)
                G.edges[a, ne]["penwidth"] = 2

                if a in set_path:
                    if ne in set_path:
                        G.nodes[a]["fillcolor"] = "blue"

                        G.nodes[ne]["fillcolor"] = "blue"
                        G.edges[a, ne]["color"] = "blue"
                        G.edges[a, ne]["penwidth"] = 3

    nx.nx_agraph.view_pygraphviz(G, prog='dot')


if __name__ == '__main__':
    n = 4
    k = 4
    m = 3
    E = generate_edge_faulty_edge_set(n, k, 0.05)
    s = get_random_node(n, k)
    t = get_random_node(n, k)
    # route = heuristic_square_routing(s, t, E, k, m)
    route = heuristic_square_routing(s, t, E, n, k, 3)
    # print_edge_path(s, t, k, route, E)
    draw_graph(s, t, n, k, route, E)

    # k = 40
    # m = 3
    # E, N = generate_node_faulty_edge_set(2, k, 0.2)
    # s = get_random_node(2, k)
    # t = get_random_node(2, k)
    # if t in N:
    #     print("t in N")
    # if s in N:
    #     print("s in N")
    # print(s, t)
    # route = heuristic_square_routing(s, t,
    #                                  E,
    #                                  k,
    #                                  m)
    # print(route)
    # print_path(s, t, k,
    #            route, N)

    # print(is_node_in_mesh((5, 7), (5,5), (10, 10), 30, 5))
    # exit()

    # print(bfs_in_mesh((5, 7), (5, 5), (10, 10),
    #                  generate_faulty_edge_set(2, 30, 0.3), 30, 5, 0))
    # exit()
    # print(get_random_node(2, 6))
    # print(generate_faulty_edge_set(2, 6, 0.1))
    # print(get_neighbors((1, 1), 2, 3))
    pass
