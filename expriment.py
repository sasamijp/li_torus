import itertools
import multiprocessing
from multiprocessing.pool import Pool
from typing import Optional, Tuple

from algorithm import generate_node_faulty_edge_set, \
    generate_edge_faulty_edge_set, get_random_node, \
    chain_routing, heuristic_square_routing, brute3_routing, \
    has_non_faulty_route, adaptive_square_routing, node_dist, get_edge_as_str

group1 = (
    (16, 5),
    (32, 5),
    (16, 4),
    (32, 4),
    (16, 3),
    (32, 3),
)

group2 = (
    (64, 8),
    (128, 8),
    (64, 7),
    (128, 7),
    (64, 6),
    (128, 6)
)

group3 = (
    tuple([(k, 3) for k in range(8, 65, 4)])
)

group4 = (
    (5, 3),
)

P = (0, 0.05, 0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.40, 0.45, 0.5)
P2 = (0.5,)
N = 10000


# k-ary, 辺故障率pのトーラスでmxmのメッシュを使って経路選択の実験をする
# 成功したら経路長、失敗したらNone(null)を返す
def get_result(n, k, m, p) -> Optional[int]:
    faulty_edges = generate_edge_faulty_edge_set(n, k, p)
    s, t = get_random_node(n, k), get_random_node(n, k)

    # O(k)で結果がわかる
    path = heuristic_square_routing(s, t, faulty_edges, n, k, m)
    #path = brute3_routing(s, t, faulty_edges, n, k)

    # アルゴリズムが非故障経路を見つけられなかった
    if path is None:
        # しかし、全探索O(k^2)すると非故障経路がある
        if has_non_faulty_route(s, t, faulty_edges, n, k):
            # これは失敗と数える
            return None

        # 全探索しても非故障経路がなかったから、もう一度最初から
        return get_result(n, k, m, p)

    return len(path) - 1


group, probs = group4, P
print("km:", group)
print("N:", N)
# print("ratio_faulty_edges", " ".join(['{:.2f}'.format(p) for p in probs]))

for n, (k, m) in itertools.product(range(2, 6), group):
    rs = []
    ls = []

    print(str(n) + "," + str(k))
    for p_faulty in probs:

        args = [(n, k, m, p_faulty) for _ in range(N)]

        with Pool(multiprocessing.cpu_count()) as pool:
            R = pool.starmap(get_result, args)

        n_reached = sum(1 for r in R if r is not None)
        reachability = n_reached / float(N)

        # n_a_reached = sum(1 for a, b in R if a is not None)
        # a_reachability = n_a_reached / N

        # n_b_reached = sum(1 for a, b in R if b is not None)
        # b_reachability = n_b_reached / N

        sum_length = sum(r for r in R if r is not None)
        average_length = sum_length / float(n_reached)
        print(p_faulty, end="")
        print("", reachability, end="")
        print("", average_length, end="")
        print()
        # print("", '{:.2f}'.format(b_reachability / a_reachability), end="")
        # print("", '{:.4f}'.format(reachability), end="")
        # rs.append(reachability)
        # ls.append(average_length)
    else:
        # print()
        pass
        # print(k, m, p_faulty, reachability, average_length)

    continue
    # print("reachability", end="")
    print(n, k, end="")
    for r in rs:
        print("", r, end="")
    # print()

    # print("average_length", end="")
    for l in ls:
        print("", round(l, 5), end="")
    print()
